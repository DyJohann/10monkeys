import tensorflow as tf
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def read_and_decode(filename, batch_size):
    # 建立文件名隊列
    filename_queue = tf.train.string_input_producer([filename],
                                                    shuffle=True,
                                                    num_epochs=None)

    # 數據讀取器
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)

    # 數據解析
    img_features = tf.parse_single_example(
        serialized_example,
        features={'Label': tf.FixedLenFeature([], tf.int64),
                  'image_raw': tf.FixedLenFeature([], tf.string), })

    image = tf.decode_raw(img_features['image_raw'], tf.uint8)
    image = tf.reshape(image, [image_size, image_size, 3])
    image = tf.cast(image, tf.float32) * (1. / 255)
    label = tf.cast(img_features['Label'], tf.int64)

    # 依序批次輸出 / 隨機批次輸出
    # tf.train.batch / tf.train.shuffle_batch
    image_batch, label_batch = tf.train.shuffle_batch(
        [image, label],
        num_threads=4,
        batch_size=batch_size,
        capacity=10000 + 3 * batch_size,
        min_after_dequeue=10000
    )

    # tf.train.shuffle_batch 重要參數說明
    # tensors：   排列的張量。
    # batch_size：從隊列中提取新的批量大小。
    # capacity：  一個整數。隊列中元素的最大數量。
    # min_after_dequeue：出隊後隊列中的最小數量元素，用於確保元素的混合級別。

    # ****************************************************************
    # Resize image (Train X)
    image_batch_train = tf.reshape(image_batch, [-1, image_size, image_size, 3])
    # One hot labeling (Trin Y)
    label_batch_train = tf.one_hot(label_batch, Label_size)

    return image_batch_train, label_batch_train


# 定義捲積層function
def conv2d(x, FilterN):
    return tf.layers.conv2d(
        inputs=x,
        filters=FilterN,
        kernel_size=[kernel_size, kernel_size],
        padding='SAME',
        activation=tf.nn.relu)


# 定義池化層function
def max_pool_2x2(x):
    return tf.layers.max_pooling2d(
        inputs=x,
        pool_size=[2, 2],
        strides=2)


def full_connect_layer(x, NeuralNum):
    return tf.layers.dense(inputs=x, units=NeuralNum, activation=tf.nn.relu)


filename = "Monkeys_Val.tfrecords"

batch_size = 32
kernel_size = 5
Label_size = 10
image_size = 128
data_lens = 272
minBtch = int(data_lens / batch_size)
ModelPath = "model\\"
LogPath = 'logs/predict/'

image_val_batch, label_val_batch = read_and_decode(filename, batch_size)
x = tf.placeholder(tf.float32, [batch_size, image_size, image_size, 3])
y_ = tf.placeholder(tf.float32, [batch_size, Label_size])
x_image = tf.reshape(x, [-1, image_size, image_size, 3])
# keep_prob = tf.placeholder(tf.float32)

with tf.name_scope('conv1'):
    Output = conv2d(x_image, 32)
    Output = conv2d(Output, 32)
    # Output = tf.nn.dropout(Output, keep_prob)

with tf.name_scope('pool1'):
    Output = max_pool_2x2(Output)

with tf.name_scope('conv2'):
    Output = conv2d(Output, 64)
    Output = conv2d(Output, 64)
    # Output = tf.nn.dropout(Output, keep_prob)

with tf.name_scope('pool2'):
    Output = max_pool_2x2(Output)

with tf.name_scope('FC'):
    Output = tf.reshape(Output, [-1, 32 * 32 * 64])
    Output = full_connect_layer(Output, 1024)
    # Output = tf.nn.dropout(Output, keep_prob)
    Output = full_connect_layer(Output, 1024)
    # Output = tf.nn.dropout(Output, keep_prob)

with tf.name_scope('softmax'):
    y_conv = tf.layers.dense(inputs=Output, units=Label_size)
    y_conv = tf.nn.softmax(y_conv)

with tf.name_scope('accuracy'):
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

saver = tf.train.Saver()

if not os.path.exists(LogPath):
    os.mkdir(LogPath)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, "model\\10MonkeysSpecies.ckpt")
    # ***************************************************
    # writer_1 = tf.summary.FileWriter(LogPath)
    # writer_1.add_graph(sess.graph)
    # writer_op = tf.summary.merge_all()
    # writer_1.flush()
    # ***************************************************
    # for i in range(data_lens):
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    for i in range(minBtch):
        image_data, label_data = sess.run([image_val_batch, label_val_batch])
        sess.run(y_conv, feed_dict={x: image_data})
        print(y_conv)

    coord.request_stop()
    coord.join(threads)

print('Done !')
