from os import walk
import os
from os.path import join
import numpy as np
import cv2

path="training\\"

for root, dirs, files in walk(path):
  for f in files:
    fullpath = join(root, f)
    img=cv2.imread(fullpath)
    print(fullpath)
    if img is None:
        # os.remove(fullpath)
        print("None !")
    else:
        height, width = img.shape[:2]
        BorderSize=0
        if (height>width):
            BorderSize=height
        else:
            BorderSize=width
        nimg = cv2.copyMakeBorder(img,int((BorderSize-height)/2),int((BorderSize-height)/2)
                                  ,int((BorderSize-width)/2),int((BorderSize-width)/2),cv2.BORDER_CONSTANT)
        sPath="Resize\\"+root
        if not os.path.isdir(sPath):
            os.mkdir(sPath)
        cv2.imwrite("Resize\\"+fullpath,nimg)